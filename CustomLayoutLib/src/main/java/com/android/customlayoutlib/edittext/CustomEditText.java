package com.android.customlayoutlib.edittext;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.customlayoutlib.R;
import com.android.customlayoutlib.util.ValidationUtils;

/**
 * Created by ThuNguyen on 5/20/2016.
 */
public class CustomEditText extends ConstraintLayout {
    protected TextView tvTitle;
    protected TextView tvError;
    protected EditText edtInput;
    protected ImageView ivClear;

    private boolean mIsEdit = true;
    private String title;
    private String hint;
    private String error;
    private boolean isEmail;
    private boolean isPassword;
    private boolean isPhoneNumber;
    private boolean isSingleLine;
    private String errorForEmpty;

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText, defStyleAttr, 0);
        title = a.getString(R.styleable.CustomEditText_edt_title);
        hint = a.getString(R.styleable.CustomEditText_edt_hint);
        error = a.getString(R.styleable.CustomEditText_edt_error);
        isEmail = a.getBoolean(R.styleable.CustomEditText_edt_is_email, false);
        isPassword = a.getBoolean(R.styleable.CustomEditText_edt_is_password, false);
        isPhoneNumber = a.getBoolean(R.styleable.CustomEditText_edt_is_phone_number, false);
        isSingleLine = a.getBoolean(R.styleable.CustomEditText_edt_single_line, true);
        a.recycle();

        errorForEmpty = getResources().getString(R.string.text_empty_error);

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        layoutInflater.inflate(R.layout.ctrl_custom_edittext, this, true);
        tvTitle = findViewById(R.id.tvTitle);
        tvError = findViewById(R.id.tvError);
        edtInput = findViewById(R.id.edtInput);
        ivClear = findViewById(R.id.ivClear);

        //ButterKnife.bind(this, this);

        if(!TextUtils.isEmpty(title)){
            tvTitle.setText(title);
            tvTitle.setVisibility(VISIBLE);
        }else{
            tvTitle.setVisibility(GONE);
        }
        if(!TextUtils.isEmpty(hint)){
            edtInput.setHint(hint);
        }
        if(isSingleLine){
            edtInput.setSingleLine(true);
            edtInput.setMaxLines(1);
        }
        if(isPassword){
            edtInput.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        if(isPhoneNumber){
            edtInput.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        // Set onclick listener
        edtInput.addTextChangedListener(mTextWatcher);
        edtInput.setOnFocusChangeListener(mOnFocusChangedListener);
        ivClear.setOnClickListener(mOnClickListener);
    }

    public void setTitle(String title){
        tvTitle.setText(title);
    }
    public void setTitle(int titleResId){
        tvTitle.setText(titleResId);
    }
    public void setError(String error){
        tvError.setText(error);
    }
    public void setError(int errorResId){
        tvError.setText(errorResId);
    }
    public void setText(String title){
        edtInput.setText(title);
    }
    public String getText(){
        return edtInput.getText().toString();
    }
    public void setVisibilityForError(int visibility){
        tvError.setVisibility(visibility);
    }
    public void setInputType(int inputType){
        edtInput.setInputType(inputType);
    }
    public void setSingleLine(boolean isSingleLine){
        edtInput.setSingleLine(isSingleLine);
    }

    public void setMinHeight(int height){
        //edtInput.setPadding(edtInput.getPaddingLeft(), edtInput.getPaddingTop(), edtInput.getPaddingRight(), height);
        edtInput.setMinHeight(height);
    }
    public void setHint(String hint){
        edtInput.setHint(hint);
    }
    public void setHint(int hintId){
        edtInput.setHint(hintId);
    }
    public void setSelection(int position){
        edtInput.setSelection(position);
    }

    public boolean isInputValid(){
        if(TextUtils.isEmpty(getText().trim())){
            tvError.setText(errorForEmpty);
            tvError.setVisibility(VISIBLE);
            return false;
        }else{
            tvError.setVisibility(GONE);
            return true;
        }
    }
    public boolean isInputAsEmail(){
        boolean isInputValid = isInputValid();
        if(isInputValid){
            if(ValidationUtils.emailValidator(getText())){
                tvError.setVisibility(GONE);
                return true;
            }
            tvError.setText(error);
            tvError.setVisibility(VISIBLE);
            return false;
        }
        return isInputValid;
    }
    public boolean isInputMatching(String match){
        if(getText().equals(match)){
            tvError.setVisibility(GONE);
            return true;
        }
        tvError.setText(error);
        tvError.setVisibility(VISIBLE);
        return false;
    }
    public void updateFieldState(boolean isEdit){
        mIsEdit = isEdit;
        edtInput.setEnabled(mIsEdit);
        if(mIsEdit){
            edtInput.setTextColor(ContextCompat.getColor(getContext(), R.color.main_color_text));
        }else{
            edtInput.setTextColor(ContextCompat.getColor(getContext(), R.color.hint_color));
        }
        if(edtInput.getText().toString().length() > 0){
            ivClear.setVisibility(mIsEdit ? VISIBLE : GONE);
        }else{
            ivClear.setVisibility(GONE);
        }
    }
    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            setVisibilityForError(GONE);
            updateFieldState(mIsEdit);
        }
    };
    private View.OnFocusChangeListener mOnFocusChangedListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(final View view, boolean focus) {
            if(view instanceof EditText){
                if(focus) {
                    ((EditText) view).setSelection(((EditText) view).getText().length());

                }
            }
//            if(mIsPriceType){
//                if(focus){
//                    String priceForEdit = edtInput.getText().toString().replaceAll(AppConstants.Currency.defaultVal().getValue(), "");
//                    edtInput.setText(priceForEdit);
//                    edtInput.setSelection(priceForEdit.length());
//                }else{
//                    String priceForDisplay = edtInput.getText().toString();
//                    if(!priceForDisplay.contains(AppConstants.Currency.defaultVal().getValue())){
//                        priceForDisplay += AppConstants.Currency.defaultVal().getValue();
//                    }
//                    edtInput.setText(priceForDisplay);
//                    edtInput.setSelection(priceForDisplay.length());
//                }
//            }

        }
    };
    private OnClickListener mOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view == ivClear){
                edtInput.setText("");
            }
        }
    };
}
