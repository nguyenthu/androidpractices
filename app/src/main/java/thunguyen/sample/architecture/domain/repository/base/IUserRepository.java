package thunguyen.sample.architecture.domain.repository.base;

import java.util.List;

import io.reactivex.Observable;
import thunguyen.sample.architecture.data.model.ws.UserWSData;
import thunguyen.sample.architecture.domain.ws.request.query.UserQuery;

/**
 * Created by Thu Nguyen on 8/23/2017.
 */

public interface IUserRepository extends IRepository {
    Observable<List<UserWSData>> getUserList();
    Observable<UserWSData> getUserDetail(UserQuery userQuery);
}
