package thunguyen.sample.architecture.domain.ws.response.user;

import thunguyen.sample.architecture.data.model.ws.UserWSData;
import thunguyen.sample.architecture.domain.ws.response.base.AbsWSResponse;

/**
 * Created by Thu Nguyen on 6/14/2018.
 */

public class UserDetailResponse extends AbsWSResponse<UserWSData> {
    @Override
    public boolean isSuccessful() {
        return false;
    }

    @Override
    public UserWSData getBodyResult() {
        return null;
    }
}
