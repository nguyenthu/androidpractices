package thunguyen.sample.architecture.domain.ws.restapi;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import thunguyen.sample.architecture.data.model.ws.UserWSData;

/**
 * Created by Thu Nguyen on 8/23/2017.
 */

public interface UserApi {
    @GET("users.json")
    Call<List<UserWSData>> getUserList();

    @GET("user_{user_id}.json")
    Call<UserWSData> getUserDetail(@Path("user_id") String userId);
}
