package thunguyen.sample.architecture.domain.ws.response.user;

import thunguyen.sample.architecture.data.model.ws.UserWSData;
import thunguyen.sample.architecture.domain.ws.response.base.AbsWSResponse;

import java.util.List;

/**
 * Created by Thu Nguyen on 6/14/2018.
 */

public class UserListResponse extends AbsWSResponse<List<UserWSData>> {
    @Override
    public boolean isSuccessful() {
        return false;
    }

    @Override
    public List<UserWSData> getBodyResult() {
        return null;
    }
}
