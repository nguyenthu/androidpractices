package thunguyen.sample.architecture.domain.ws.request.query.base;

import java.util.Map;

/**
 * Created by Thu Nguyen on 8/22/2017.
 */

public interface IWSQuery {
    Map<String, String> getMap();
}
