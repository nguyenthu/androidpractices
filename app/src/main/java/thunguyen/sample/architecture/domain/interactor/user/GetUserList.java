package thunguyen.sample.architecture.domain.interactor.user;

import thunguyen.sample.architecture.data.model.ws.UserWSData;
import thunguyen.sample.architecture.domain.interactor.base.AbsUseCase;
import thunguyen.sample.architecture.domain.repository.UserRepositoryImpl;
import thunguyen.sample.architecture.domain.repository.base.IUserRepository;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Thu Nguyen on 8/23/2017.
 */

public class GetUserList extends AbsUseCase<List<UserWSData>, Void, Void, Void> {
    IUserRepository repository;

    public GetUserList() {
        repository = new UserRepositoryImpl();
    }

    @Override
    public Observable<List<UserWSData>> buildUseCaseSingle() {
        return repository.getUserList();
    }
}
