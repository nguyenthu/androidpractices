package thunguyen.sample.architecture.domain.ws.request.query;

import thunguyen.sample.architecture.domain.ws.request.query.base.AbsWSQuery;

/**
 * Created by Thu Nguyen on 8/23/2017.
 */

public class UserQuery extends AbsWSQuery {
    String id;

    public String getId() {
        return id;
    }

    public UserQuery setId(String id) {
        this.id = id;
        return this;
    }
}
