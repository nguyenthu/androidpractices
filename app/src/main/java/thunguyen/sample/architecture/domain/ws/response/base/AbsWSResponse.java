package thunguyen.sample.architecture.domain.ws.response.base;

/**
 * Created by Thu Nguyen on 8/22/2017.
 */

public abstract class AbsWSResponse<T> implements IWSResponse {
    public abstract T getBodyResult();
}
