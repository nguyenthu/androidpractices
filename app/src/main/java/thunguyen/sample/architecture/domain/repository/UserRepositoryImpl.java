package thunguyen.sample.architecture.domain.repository;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import thunguyen.sample.architecture.data.model.ws.UserWSData;
import thunguyen.sample.architecture.domain.repository.base.AbsRepository;
import thunguyen.sample.architecture.domain.repository.base.IUserRepository;
import thunguyen.sample.architecture.domain.retro2client.AppHttpClient;
import thunguyen.sample.architecture.domain.ws.request.query.UserQuery;

/**
 * Created by Thu Nguyen on 8/23/2017.
 */

public class UserRepositoryImpl extends AbsRepository implements IUserRepository {
    @Override
    public Observable<List<UserWSData>> getUserList() {
        Call<List<UserWSData>> request = AppHttpClient.getInstance().getUserApi()
                .getUserList();
        return processResponse(request);
    }

    @Override
    public Observable<UserWSData> getUserDetail(UserQuery userQuery) {
        Call<UserWSData> request = AppHttpClient.getInstance().getUserApi()
                .getUserDetail(userQuery.getId());
        return processResponse(request);
    }
}
