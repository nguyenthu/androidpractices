package thunguyen.sample.architecture.domain.interactor.user;

import thunguyen.sample.architecture.data.model.ws.UserWSData;
import thunguyen.sample.architecture.domain.interactor.base.AbsUseCase;
import thunguyen.sample.architecture.domain.repository.UserRepositoryImpl;
import thunguyen.sample.architecture.domain.repository.base.IUserRepository;
import thunguyen.sample.architecture.domain.ws.request.query.UserQuery;

import io.reactivex.Observable;

/**
 * Created by Thu Nguyen on 8/23/2017.
 */

public class GetUserDetail extends AbsUseCase<UserWSData, UserQuery, Void, Void> {
    IUserRepository repository;

    public GetUserDetail() {
        repository = new UserRepositoryImpl();
    }

    @Override
    public Observable<UserWSData> buildUseCaseSingle() {
        return repository.getUserDetail(queryReq);
    }
}
