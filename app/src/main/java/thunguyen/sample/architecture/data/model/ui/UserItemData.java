package thunguyen.sample.architecture.data.model.ui;

import thunguyen.sample.architecture.data.model.ui.base.AbsItemData;
import thunguyen.sample.architecture.data.model.ui.base.IItemData;
import thunguyen.sample.architecture.data.model.ws.UserWSData;

/**
 * Created by Thu Nguyen on 8/24/2017.
 */

public class UserItemData extends AbsItemData<UserWSData> implements IItemData<UserItemData, UserWSData> {
    String id;
    String name;
    String email;
    String phone;
    String avatar;

    public UserItemData(UserWSData userWSData) {
        super(userWSData);
        castFromWSData(userWSData);
    }

    @Override
    public UserItemData castFromWSData(UserWSData userWSData) {
        if(userWSData != null){
            id = userWSData.getId();
            name = userWSData.getFullName();
            email = userWSData.getEmail();
            phone = userWSData.getDescription();
            avatar = userWSData.getCoverUrl();
        }
        return this;
    }

    public String getId() {
        return id;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }
}
