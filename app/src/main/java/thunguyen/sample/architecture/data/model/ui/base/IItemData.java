package thunguyen.sample.architecture.data.model.ui.base;

/**
 * Created by Thu Nguyen on 8/22/2017.
 */

public interface IItemData<P1, P2> {
    P1 castFromWSData(P2 p2);
}
