package thunguyen.sample.architecture.data.storage.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import thunguyen.sample.architecture.data.model.ws.UserWSData;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;
import static thunguyen.sample.architecture.data.storage.local.AppDB.User.queryAll;

/**
 * Created by Thu Nguyen on 9/28/2017.
 */

@Dao
public interface UserDAO {
    @Insert(onConflict = REPLACE)
    void upsertUser(List<UserWSData> userWSDataList);

    @Query(queryAll)
    List<UserWSData> getUserList();
}
