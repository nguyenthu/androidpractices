package thunguyen.sample.architecture.data.model.ws;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import thunguyen.sample.architecture.data.model.ws.base.AbsWSData;
import thunguyen.sample.architecture.data.storage.local.AppDB;

/**
 * Created by Thu Nguyen on 8/23/2017.
 */
@Entity(tableName = AppDB.User.TABLE_NAME)
public class UserWSData extends AbsWSData {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    String id;

    @ColumnInfo(name = "full_name")
    String fullName;

    @ColumnInfo(name = "email")
    String email;

    @ColumnInfo(name = "description")
    String description;

    @ColumnInfo(name = "cover_url")
    String coverUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }
}
