package thunguyen.sample.architecture.data.storage.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import thunguyen.sample.architecture.MainApplication;
import thunguyen.sample.architecture.data.model.ws.UserWSData;
import thunguyen.sample.architecture.data.storage.local.dao.UserDAO;

/**
 * Created by Thu Nguyen on 9/28/2017.
 */

@Database(version = 1, entities = {UserWSData.class})
public abstract class AppDB extends RoomDatabase{
    private static final String DB_NAME = "android_practice";

    private static AppDB instance;
    public static AppDB getInstance(){
        if(instance == null){
            instance = Room.databaseBuilder(MainApplication.getInstance().getApplicationContext(),
                    AppDB.class, DB_NAME).build();
        }
        return instance;
    }
    /************************** Define some DAO methods ************************************/
    public abstract UserDAO userDAO();

    /************************** Define some class as table ***********************************/
    public class User{
        public static final String TABLE_NAME = "user";
        public static final String queryAll = "select * from " + TABLE_NAME;
    }
}
