package thunguyen.sample.architecture.presentation.presenter;

import thunguyen.sample.architecture.data.model.ui.UserItemData;
import thunguyen.sample.architecture.presentation.presenter.base.IVP;

import java.util.List;

/**
 * Created by Thu Nguyen on 5/24/2018.
 */

public interface IUserPresenter {
    interface View extends IVP.View {
        void getUserListResponse(List<UserItemData> userItemDataList, String error);
        void getUserDetailResponse(UserItemData userItemData, String error);
    }
    interface Presenter extends IVP.Presenter{
        void getUserList();
        void getUserDetail(String userId);
    }
}
