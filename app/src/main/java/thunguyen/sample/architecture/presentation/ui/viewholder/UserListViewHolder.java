package thunguyen.sample.architecture.presentation.ui.viewholder;

import thunguyen.sample.architecture.R;
import thunguyen.sample.architecture.data.model.ui.UserItemData;
import thunguyen.sample.architecture.presentation.ui.viewholder.base.AbsRecyclerViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;

/**
 * Created by Thu Nguyen on 6/15/2018.
 */

public class UserListViewHolder extends AbsRecyclerViewHolder<UserItemData>{
    @BindView(R.id.ivUserAvatar)
    ImageView ivUserAvatar;
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    @BindView(R.id.tvUserEmail)
    TextView tvUserEmail;
    @BindView(R.id.tvUserPhone)
    TextView tvUserPhone;

    public UserListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bind(UserItemData itemdata, int pos) {
        super.bind(itemdata, pos);

        Glide.with(itemView.getContext())
                .load(itemdata.getAvatar())
                .error(R.mipmap.ic_user_default)
                .skipMemoryCache(false)
                .dontTransform()
                .dontAnimate()
                .into(ivUserAvatar);
        tvUserName.setText(itemdata.getName());
        tvUserEmail.setText(itemdata.getEmail());
        tvUserPhone.setText(itemdata.getPhone());
    }
}
