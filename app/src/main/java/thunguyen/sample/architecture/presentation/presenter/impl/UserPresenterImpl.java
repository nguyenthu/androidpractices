package thunguyen.sample.architecture.presentation.presenter.impl;

import thunguyen.sample.architecture.data.model.ui.UserItemData;
import thunguyen.sample.architecture.domain.interactor.user.GetUserDetail;
import thunguyen.sample.architecture.domain.interactor.user.GetUserList;
import thunguyen.sample.architecture.domain.ws.request.query.UserQuery;
import thunguyen.sample.architecture.presentation.presenter.IUserPresenter;
import thunguyen.sample.architecture.presentation.presenter.base.BasePresenter;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Thu Nguyen on 5/24/2018.
 */

public class UserPresenterImpl extends BasePresenter<IUserPresenter.View> implements IUserPresenter.Presenter {
    GetUserDetail getUserDetail;
    GetUserList getUserList;

    public UserPresenterImpl(IUserPresenter.View view) {
        super(view);
        getUserList = new GetUserList();
        getUserDetail = new GetUserDetail();
    }

    @Override
    public void getUserList() {
        Disposable disposable = getUserList.buildUseCaseSingle()
                .flatMap(userWSDataList ->
                        Observable.fromIterable(userWSDataList)
                                .map(item -> new UserItemData(item))
                                .toList()
                                .toObservable() // Required for RxJava 2.x
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userItemDataList -> view.getUserListResponse(userItemDataList, null),
                        throwable -> view.getUserListResponse(null, throwable.getMessage())
                );
        compositeDisposable.add(disposable);
    }

    @Override
    public void getUserDetail(String userId) {
        getUserDetail.query(new UserQuery().setId(userId));
        Disposable disposable = getUserDetail.buildUseCaseSingle()
                .flatMap(userWSData ->
                        Observable.just(new UserItemData(userWSData))// Required for RxJava 2.x
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userItemData -> view.getUserDetailResponse(userItemData, null),
                        throwable -> view.getUserDetailResponse(null, throwable.getMessage())
                );
        compositeDisposable.add(disposable);
    }
}
