package thunguyen.sample.architecture.presentation.ui.activity.base;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by Thu Nguyen on 8/18/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
}
