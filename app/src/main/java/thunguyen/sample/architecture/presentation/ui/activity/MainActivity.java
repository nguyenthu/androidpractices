package thunguyen.sample.architecture.presentation.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import thunguyen.sample.architecture.R;
import thunguyen.sample.architecture.data.model.ui.UserItemData;
import thunguyen.sample.architecture.presentation.presenter.IUserPresenter;
import thunguyen.sample.architecture.presentation.presenter.impl.UserPresenterImpl;
import thunguyen.sample.architecture.presentation.ui.activity.base.BaseActivity;
import thunguyen.sample.architecture.presentation.ui.adapter.UserListAdapter;

public class MainActivity extends BaseActivity implements IUserPresenter.View {
    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    UserListAdapter userListAdapter;

    IUserPresenter.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("User List");
        ButterKnife.bind(this, this);
        rvUserList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        userListAdapter = new UserListAdapter();

        // Call user list
        presenter = new UserPresenterImpl(this);
        presenter.getUserList();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    @Override
    public void getUserListResponse(List<UserItemData> userItemDataList, String error) {
        if(TextUtils.isEmpty(error)){
            Log.d("Thu Nguyen", "userItemDataList = " + userItemDataList.size());
            userListAdapter.setDataList(userItemDataList);
            rvUserList.setAdapter(userListAdapter);
        }
    }

    @Override
    public void getUserDetailResponse(UserItemData userItemData, String error) {

    }
}
