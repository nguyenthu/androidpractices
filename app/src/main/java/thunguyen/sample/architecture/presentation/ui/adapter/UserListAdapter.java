package thunguyen.sample.architecture.presentation.ui.adapter;

import thunguyen.sample.architecture.R;
import thunguyen.sample.architecture.data.model.ui.UserItemData;
import thunguyen.sample.architecture.presentation.ui.adapter.base.AbsRecyclerViewAdapter;
import thunguyen.sample.architecture.presentation.ui.viewholder.UserListViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;

/**
 * Created by Thu Nguyen on 6/15/2018.
 */

public class UserListAdapter extends AbsRecyclerViewAdapter<UserItemData, UserListViewHolder>{
    @Override
    public UserListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.ctrl_user_item, parent, false));
    }

    @Override
    public void onBindViewHolder(UserListViewHolder holder, int position) {
        holder.bind(getItem(position), position);
    }
}
